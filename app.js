const express = require("express");
const https = require("https");
const bodyParser = require("body-parser");
const app = express();
const port = 3000;

app.use(bodyParser.urlencoded({extended: true}));
app.get("/", (req,res) => {
  console.log('wass really up?');
  res.sendFile(__dirname + '/index.html');
});

app.post("/", (req,res) => {
  //console.log('wassup?');
  //console.log("cityName: " + req.body.cityName);
  gogetem(res,req.body.cityName);
}).on('error', (e) => {
  console.error(e);
});

function gogetem(res,q) {
  //main.temp Temperature. Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.

  const query = q; //'Tampa';
  const unit = 'Imperial';
  const apiKey = 'd15b18ea7d2e31226ffd91093153f1e8';
  const url = `https://api.openweathermap.org/data/2.5/weather?q=${query}&appid=${apiKey}&unit=${unit}`;

  https.get(url, (response) => {
    console.log(response.statusCode);
    console.log('headers:', response.headers);
    response.on('data', (d) => {
      //process.stdout.write(d);
      weatherData = JSON.parse(d);
      //console.log(weatherData);
      const weather = weatherData.weather[0].description;
      const temp = parseInt((parseInt(weatherData.main.temp) * 9 / 5) - 459.67);
      const icon = weatherData.weather[0].icon;
      const iconUrl = `https://openweathermap.org/img/wn/${icon}@2x.png`;
      res.write(`<h1>The weather in ${query} is ${weather}.</h1>`);
      res.write(`<h3>The temperature in ${query} is ${temp} degrees Farenheit.</h3>`);
      res.write(`<img src='${iconUrl}' alt='node it'>`);
      res.send();
    });
  }).on('error', (e) => {
    console.error(e);
  });
}

app.listen(port, () => console.log(`Server is running on port ${port}.`));
